+++
title = "Getting started"
description = "Java module that runs JMeter tests against a microservice deployment"
chapter = true
weight = 1
pre = "<b>1. </b>"
+++

# Getting started

Get a brief overview about the purpose and different modules inside this project