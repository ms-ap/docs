+++
title = "msap-test"
weight = 3
description = "Java program to run JMeter tests against containerized applications"
+++
---
## Description
[msap-test]({{< msap-test >}}) is a Java application to control [JMeter](https://jmeter.apache.org/) performance tests for microservices in a kubernetes / docker swarm cluster.

## Prerequisites
- [JDK 11](https://www.oracle.com/technetwork/java/javase/downloads/jdk11-downloads-5066655.html) or newer
- [Apache Maven](https://maven.apache.org/download.cgi)
- [git](https://git-scm.com/downloads)
- [docker](https://docs.docker.com/install)
- [ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html) 2.7

### Included tests
Currently we have tests for

- a static website (for example nginx or apache)
- wordpress with database (mariadb and percona xtradb)
- gitlab with
  - ssh
  - container registry
- rocketchat with mongodb
- job (like described [here](https://kubernetes.io/docs/concepts/workloads/controllers/jobs-run-to-completion/))

### How it works
We use a [Java]({{< msap-test >}}/msapjmetertest) program to execute the following tasks until a specific amount of tests were run

1. run the site playbook from [msap-ansible]({{% ref "modules/msap-ansible" %}}) to deploy the application to kubernetes / docker swarm
1. wait for the application to become ready
1. run the JMeter test against the deployed application
1. clean up the deployment
1. wait a few seconds

{{< figure src = "/docs/images/Java_UML_en.png" title = "msap-test overview">}}