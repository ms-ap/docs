+++
title = "Usage"
description = "Usage of msap-manager"
weight = 2
+++
---
First you should install the requirements via `pip install -r requirements.txt` from within the project directory.
If you adjusted the settings to your needs like described in the [settings section]({{< ref "./settings.md" >}}) you can either run 
```sh
ansible-playbook site.yml -i inventories/production --user remoteUserName --private-key /path/to/private_key/for/remoteUser
```

or if you followed [Ansible specific settings]({{< ref "./settings.md#ansible-specific-settings" >}}) just

```sh
ansible-playbook site.yml
```

to bootstrap a kubernetes / docker swarm cluster.