+++
title = "msap-ansible"
weight = 1
description = "Ansible modules for deployment of kubernetes or docker swarm"
+++
---
## Description
[msap-ansible]({{< msap-ansible >}}) is an ansible playbook to bootstrap a kubernetes / docker swarm cluster.

## Prerequisites
- see [requirements.txt]({{< msap-ansible >}}/requirements.txt)
- run `pip install -r requirements.txt`

## Playbooks
playbook | description
---------|------------
[kubernetes.yml]({{< msap-ansible >}}/kubernetes.yml) | Bootstraps a cluster with kubernetes by using kubeadm. This also installs a CNI and CRI which can be configured by a user.
[swarm.yml]({{< msap-ansible >}}/swarm.yml) | Bootstraps a cluster with docker swarm.
[site.yml]({{< msap-ansible >}}/site.yml) | Includes either kubernetes or docker swarm playbook depending on the users settings. Installs monitoring if `monitoring.enabled` was set by the user.
[reset_site.yml]({{< msap-ansible >}}/reset_site.yml) | Resets the cluster and deletes all containers. You can run the site playbook again after this one to rebuild the cluster.
[packer.yml]({{< msap-ansible >}}/packer.yml) | Special playbook and default execution on localhost to just install the needed software but without configuration.
[test.yml]({{< msap-ansible >}}/test.yml) | Only used for CI tests.

## Roles
{{% notice tip %}}
You can look into echo role in the [roles directory]({{< msap-ansible >}}/roles) to see additional information
like used variables and a description of each role.
{{% /notice %}}
