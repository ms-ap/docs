+++
title = "Settings"
description = "Settings for msap-ansible"
weight = 1
+++
---
## group_vars
To avoid a duplicate documentation the group_vars are documented directly in the `.msap-ansible` block [here]({{< msap-manager >}}/settings.yml.sample).
Default values are stored in the [group_vars file]({{< msap-ansible >}}/inventories/production/group_vars/all.yml.sample).

## Inventory
group | description
------|------------
master | IP address of the master (currently only a single master is supported)
nodes | IP adresses for the nodes
hosts:children | Should always include `master` and `nodes` (i. e. all managed hosts)

## Ansible specific settings
You may want to set some default settings for ansible like a private key which should be used to connect to all your hosts via ssh.
This can be done in several ways but the easiest might be to use a ansible.cfg file to apply your settings system wide.
You can find an [example file]({{< msap-manager >}}/ansible/ansible.cfg) that should work for you
in the [msap-manager]({{% ref "modules/msap-manager" %}}) project.

If you use this file as your template you must copy it to the [projects root directory]({{< msap-ansible >}})
and you most likely need to change
```ini
[defaults]
inventory = /path/to/your/inventory
remote_user = username of your remote user you want to connect to via ssh
private_key_file = /path/to/the/private/key/that/matches/the/remote_users/public/key
```