+++
title = "Architecture" 
description = "Project overview"
weight = "1"
+++
---
If you use [msap-ansible]({{< ref "modules/msap-ansible" >}}) for the deployment you'll get the following architecture

{{< figure src = "/docs/images/msap-manager-arch.png" title = "msap-manager architecture">}}