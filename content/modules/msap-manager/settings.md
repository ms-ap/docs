+++
title = "Settings"
description = "Settings for msap-manager"
weight = 2
+++
---
You can change the default settings if you rename [settings.yml.sample]({{< msap-manager >}}/settings.yml.sample) to settings.yml. This file also contains explanations for the settings.
If you are happy with the default values you don't need to da anything, because `msap.py` will simply copy the sample file for you.

The settings will be automatically distributed to:

- the [ansible directory]({{< msap-manager >}}/ansible) which contains 
  - the ansible configuration file (ansible/ansible.cfg),
  - the ansible inventory (ansible/hosts) and the
  - ansible group_variables (ansible/group_vars/all)
- the vagrant variables file (vagrant_variables.yml)

{{% notice tip %}}
If you want to supply some additional settings for {{< ceph-ansible >}} you should have a look at their docs.
{{% /notice %}}

{{% notice info %}}
Some of the files may not exist if you have never run `msap.py` as it generates the files on first run.
{{% /notice %}}

{{% notice warning %}}
Users should not edit one of those files by hand!
{{% /notice %}}

The settings are stored in YAML format and are devided into 3 groups:

- shared settings, which will be used in vagrant and ansible
- vagrant settings, which will be used only for Vagrant tasks (i. e. VM parameters and provisioning scripts)
- ansible settings, which will be mainly used for ansible playbooks

### Additional Vagrant settings
Users can put Vagrant boxfiles into the [boxes]({{< msap-manager >}}/boxes) directory and start the cluster with the custom box by using the following command `python msap.py start --box netlab/<BOXNAME>`. 
This will import the box into Vagrant and provision the VMs using the new box. 
A pre-provisioned Box could be useful if you want to save some time since you can pre-load some software dependencies or container images.

Users can put their ssh public key(s) into the [ssh]({{< msap-manager >}}/ssh) directory. 
The keys **must** have the '.pub' extension! All keys with the '.pub' ending will be appended to the `authorized_keys` file inside the VMs.
