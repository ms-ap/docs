+++
title = "msap-manager"
weight = 2
description = "Python wrapper for vagrant to deploy a test environment on a local machine"
+++
---
## Description
[msap-manager]({{< msap-manager >}}) is a python wrapper for Vagrant to start a virtual kubernetes / docker swarm cluster.

## Supported operating systems
msap-manager was tested on the following hosts:

- Windows 10 1803+
- Debian Stretch
- Ubuntu 1604
- macOS High Sierra

Generally all host OS that meet the prerequisites should work.

## Prerequisites
The following packages are required to run msap-manager:

- [VirtualBox](https://www.VirtualBox.org/wiki/Downloads)
- [Vagrant](https://www.vagrantup.com/downloads.html) version 2 or newer
- [Python3.7](https://www.python.org/downloads)
- [pip](https://packaging.python.org/tutorials/installing-packages/#requirements-for-installing-packages)
- [pipenv](https://pipenv.readthedocs.io/en/latest/install)
- [git](https://git-scm.com/downloads)
- 8 GB RAM or more
- CPU with Intel (VT-x) or AMD (AMD-V) support

For most Linux systems simply run:

```sh
sudo apt install python3 \
  virtualbox \
  vagrant &&
  pip install pipenv
```

In the project directory you have to run
```sh
pipenv install # This command is only needed ONCE or if you are getting ModuleNotFoundError
```
to install dependencies in a virtual environment.

## Restrictions

There is a bug when running VirtualBox versions newer than 5.2.6 inside VMware with Windows as guest. 
If you want to try out things inside a VMware Windows VM use [VirtualBox 5.2.6](https://download.virtualbox.org/virtualbox/5.2.6/VirtualBox-5.2.6-120293-Win.exe) or older in this VM. 
Running VirtualBox directly on the Windows host is not affected by this!

When enabling kubectl proxy via settings it is not restarted automatically. If you restart the master you have to manually restart the proxy with the following command from inside the master. 
```sh
kubectl proxy --port 8001 --address 192.168.90.2 --accept-hosts '^*$' >/dev/null 2>&1 &
```

## Design decisions
#### Why we are not using the ansible provisioner from Vagrant

If you had a closer look on how the python wrapper works you may have noticed that it generates some files needed for ansible and that ansible is installed inside master VM.

Vagrant provides a [provisioner for ansible environments](https://www.vagrantup.com/docs/provisioning/ansible.html). However this provisioner requires ansible to be installed on the host OS.
Since there is currently no easy way to install an ansible master on a Windows host This would defeat the goal of making this project as cross platform as possible.

Also some ansible roles may require different ansible versions. So the user would have to switch between versions.
With ansible installed inside a VM we could control the needed versions without polluting the users host OS.
