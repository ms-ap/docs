+++
title = "Usage"
description = "Usage of msap-manager"
weight = 2
+++
---
Before you start [msap-ansible]({{< msap-ansible >}}) the first time you have to run the following command.
This will install the dependencies from pypy and is only needed ONCE or if you are getting ModuleNotFoundError.
```sh
pipenv install
```


```sh
pipenv run python msap.py [-h] [-q] [--debug] {start,stop,resume,restart,reset,rebuild,clean,ls,ssh}
```

positional arguments:

| Argument | Description |
| ----- | ----- |
| start | start the virtual cluster
| stop | stop the virtual cluster
| resume | start a previously stopped virtual cluster
| restart | restart virtual machines and load new Vagrantfile configuration
| reset | reset the kubernetes cluster inside the virtual machines without changing the virtual machines running state
| rebuild | rebuild the kubernetes cluster inside the virtual machines (should be run after reset)
| clean | kill all virtual machines in the cluster and delete them
| ls | list the virtual machines and their state
| ssh | connect to a virtual machine via ssh
| config | shows or generates config files (i. e. ansible group_vars)

optional arguments:

| Argument | Description |
| ------ | ------ |
| -h, --help | show this help message and exit
| -q, --quiet | log only warnings and errors 

For subcommands see 
```sh
python msap.py [ARG] -h
```

If you provide command line arguments they will override the default settings temporarily.

## Examples
### msap

Build a cluster with 1 node
```sh
pipenv run python msap.py start
```

Build a cluster with 1 node and ceph enabled for persistent storage
```sh
pipenv run python msap.py start --ceph
```

Build a cluster with 3 nodes
```sh
pipenv run python msap.py start --node-count 3
```

Build a cluster with a specific CNI
```sh
pipenv run python msap.py start --cni flannel
```

Build a cluster with a packet capturing enabled
(the .pcap file will be stored in the users home directory)
```sh
pipenv run python msap.py start --packet-capture
```

Delete a cluster
```sh
pipenv run python msap.py reset
```

Rebuild a cluster with a new cni
```sh
pipenv run python msap.py rebuild --cni flannel
```

Clean up (delete all VMs)
```sh
pipenv run python msap.py clean
```

Print help for start subcommand
```sh
pipenv run python msap.py start -h
```

All of the above works with debug mode like
```sh
pipenv run python msap.py --debug start
```

### kubectl
Once you have built a cluster with 
```sh
pipenv run python msap.py start
```

you can ssh into the master with
```sh
pipenv run python msap.py ssh master
```

Now you can use `kubectl` commands to play with the kubernetes cluster. You can create a nginx deployment with the following command
```sh
kubectl apply -f https://k8s.io/examples/application/deployment.yaml
```

for more information check the [nginx example from the official kubernetes docs](https://kubernetes.io/docs/tasks/run-application/run-stateless-application-deployment/) and the other examples in the docs.
