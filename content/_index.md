# MSAP

MSAP stands for MicroService Architecture Playground. It provides several modules to deploy a microservice via the
orchestration frameworks [kubernetes](https://kubernetes.io/) and [docker swarm](https://docs.docker.com/engine/swarm/).

### Features
- Build your own orchestration test environment
- Fully open source

### Contents
{{% children depth = "2" %}}